# pdal

Housing of PDAL images useful set up as a one shot set of tools

## Description
This repository offers access to the [PDAL library](http://pdal.io/index.html). It encapsulates this great work into a docker setup ready to use. There are several docker setups out there. However this repository tries to offer a wide range of drivers as well as a small build to keep it shippable. In addition it encapsultes helpful tools in the images to handle the heavy point cloud data.

## Usage
This repository drives version branches. Each branch provides access on a version PDAL. To get information of the used libs and offered drivers refer to the readme in the desired version branch.

## Support
Support for this repository might be asked via a [new issue ](https://gitlab.com/rudert-geoinformatik/docker-tools/pdal/-/issues/new)

## Roadmap
I try to maintain this in the future with up to date versions of PDAL.

## Contributing
Contribution on this is welcome. However since this is offering a narrow usecase I need to avoid spreading of the project.

## Authors and acknowledgment
This work is only possible due to the outhors of the PDAL/GDAL libs as well as the others it depends on. So all credits go to them.

## Project status
This project is updated regularly with newer versions of PDAL and the corresponding libs. It might also be extended by with more drivers on request. See Support section for further details.
